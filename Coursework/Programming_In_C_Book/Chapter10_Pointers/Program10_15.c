
// function to count characters in a string 

#include <stdio.h>

int stringLength (const char *string)
{
    const char *cptr = string;

    while (*cptr)
        ++cptr;
    
    //return an int counting the number of elements NOT INCLUDING NULL CHAR
    return cptr - string;
}

int main (void)
{
    int stringLength(const char *string);

    printf("%i ", stringLength("stringLength test"));
    printf("%i ", stringLength(""));
    printf("%i\n", stringLength("complete"));
}