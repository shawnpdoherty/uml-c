// program to update the time by one second
#include <stdio.h>

struct time
{
    int hour;
    int minute;
    int seconds;
};

int main(void)
{
    struct time timeUpdate(struct time now);
    struct time currentTime, nextTime;

    printf("Enter the time (hh:mm:ss): ");
    scanf("%i:%i:%i", &currentTime.hour, & currentTime.minute, & currentTime.seconds);

    nextTime = timeUpdate(currentTime);


    printf("Updated time is %.2i:%.2i:%.2i.\n", nextTime.hour, nextTime.minute, nextTime.seconds);
    return 0;
}

//function to update the time by one second
struct time timeUpdate(struct time now)
{
    ++now.seconds;
    
    if (now.seconds == 60) // next minute
    {
        now.seconds = 0;
        ++now.minute;
        
        if (now.minute == 60) // next hour
        {
            now.minute = 0;
            ++now.hour;

            if (now.hour == 24) // midnight
                now.hour = 0;
        }       

    }

    return now;
}